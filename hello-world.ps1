[CmdletBinding()]
param (
    [Parameter(Mandatory=$false)]
    [string]
    $name
)

if ($name.Length -eq 0) {
    $name = "World"
}

$entry = "$(Get-Date -Format "yyyy-MM-dd HH:mm:ss") - Hello $name!"
$entry | Out-File -FilePath C:\Temp\gitOutput.txt -NoClobber -Append
